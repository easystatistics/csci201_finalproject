package client;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import com.opencsv.CSVWriter;

import statistics.VariableList;
import statistics.VariableList.Variable;

public class CSVExporter {
	
	public static void parseVariableList(File file, ProjectArea projectArea) {
		if (file == null) {
			// no file
			projectArea.display(projectArea.commandController.defaultPanel("Parse Error", "Please enter a valid file", true));
			return;
		}
		if (!file.getName().endsWith(".csv")) {
			// bad file extension
			projectArea.display(projectArea.commandController.defaultPanel("Parse Error", file.getName()+" is not a csv file.", true));
			return;
		}
		CSVWriter w = null;
		try {
			w = new CSVWriter(new FileWriter(file), ',');
			CSVExporter.parseVariableListHelper(projectArea, w);
		} catch (IOException e) {
			// File exception
			projectArea.display(projectArea.commandController.defaultPanel("Parse Error", "Error reading from "+file.getName()+".", true));
			return;
		} finally {
			if (w != null) {
				try {
					w.close();
				} catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
		}
	}
	
	public static void parseVariableListHelper(ProjectArea projectArea, CSVWriter w) {
		VariableList variables = projectArea.getVariables();
		ArrayList<String> variableNames = variables.getVariableNames();
		String variableNamesArray[] = variableNames.toArray(new String[variableNames.size()]);
		w.writeNext(variableNamesArray);
		for (int i = 0; i < variableNames.size(); i++) {
			Variable v = variables.get(i);
			ArrayList<Double> values = v.returnValues();
			Double valuesArray[] = values.toArray(new Double[values.size()]);
			String[] writeLine = new String[values.size() + 1];
			writeLine[0] = v.returnName();
			for (int j = 1; j < values.size() + 1; j++) {
				if(valuesArray[j-1]!=null) {
					writeLine[j] = valuesArray[j-1].toString();
				} else {
					writeLine[j] = "";
				}
			}
			w.writeNext(writeLine);
		}
	}
}
