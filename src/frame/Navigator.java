package frame;

import server.UserInfo;

public interface Navigator {
	public void toMain(boolean online);
	public void toLogin();
	public void toSignup();
	public void sendInfo(UserInfo info);
	public void loginSuccess(String message);
	public void loginFailure(String message);
	public void signupSuccess(String message);
	public void signupFailure(String message);
}
