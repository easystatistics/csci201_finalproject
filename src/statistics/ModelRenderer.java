package statistics;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import resources.Constants;
import statistics.ModelList.AbstractModel;

public class ModelRenderer extends JLabel implements ListCellRenderer<AbstractModel> {
	private static final long serialVersionUID = 1L;
	private ModelList ml;
	public ModelRenderer(ModelList ml) { 
	    setOpaque(true); 
	    this.ml = ml;
	}


	@Override
	public Component getListCellRendererComponent(JList<? extends AbstractModel> list, AbstractModel model, int index, boolean isSelected,
			boolean cellHasFocus) {

		if(!ml.current.contains(model)) {
			setForeground(list.getBackground());
			//return this;
		} else if(model.hasNull()) {
			setForeground(Constants.NULL_VALUES_COLOR);
		} else if (model.hasDifferentVariableLengths()) {
			setForeground(Constants.INCONSISTENT_MODEL_COLOR);
		} else {
			setForeground(list.getForeground());
		}
		
		if (isSelected && ml.current.contains(model)) {
		    setBackground(list.getSelectionBackground()); 
		} else { 
		    setBackground(list.getBackground()); 

		}
		
		setText("  "+model.toString());
		return this;
	}
}
