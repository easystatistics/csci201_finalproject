package server;

import java.io.Serializable;

public class SuccessMessage implements Serializable{
	public static final long serialVersionUID = 1;
	private String message;
	public Type1 type1;
	public Type2 type2;
	
	public enum Type1 {
		SUCCESS, FAILURE
	}
	
	public enum Type2 {
		LOGIN, SIGNUP
	}
	
	public SuccessMessage(String message, Type1 type1, Type2 type2) {
		this.message = message;
		this.type1 = type1;
		this.type2 = type2;
	}
	
	public String getMessage() {
		return message;
	}
}
