CREATE DATABASE IF NOT EXISTS easystatisticsDB;

USE easystatisticsDB;
CREATE TABLE IF NOT EXISTS `easystatisticsDB`.`users` (
  `userid` INT AUTO_INCREMENT PRIMARY KEY,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `hosting` BOOL NOT NULL,
  UNIQUE INDEX `username_UNIQUE` (`username` ASC))

DEFAULT CHARACTER SET = utf8;