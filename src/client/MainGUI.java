package client;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

public class MainGUI extends JFrame {
	private static final long serialVersionUID = 1L;
	public static JTabbedPane tabbedPane;
	public static JMenuItem newFileOption;
	public static JMenuItem importFromFileOption;
	public static JMenuItem openProjectAreaOption;
	public static JMenuItem signinOption;
	public static JMenuItem signoutOption;
	public static JMenuItem connectOption;
	public static JMenuItem closeOption;
	public static JMenuItem exportCSVOption;
	public static JMenuItem saveProjectAreaOption;
	public static JMenu openHelpMenu;
	public static JMenu openSettingsMenu;
	public static JMenu networkMenu;
	public static HelpMenuGUI helpMenuGUI;
	public static GlobalOptionsGUI settingsGUI;
	static ClientListener client;
	private boolean online = false;

	public MainGUI(ClientListener c) {
		super("Easy Statistics Software");
		client = c;
		if(client!=null)online = true;
		setSize(900, 600);
		setLocationRelativeTo(null);
		initTabbedPane();
		initMenu();
		helpMenuGUI = new HelpMenuGUI(this);
		settingsGUI = new GlobalOptionsGUI(this);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	private void initTabbedPane() {
		tabbedPane = new JTabbedPane();
		add(tabbedPane, BorderLayout.CENTER);
	}
	
	public static ProjectArea getActiveProjectArea() {
		int selected = tabbedPane.getSelectedIndex();
		if (selected == -1) {
			return null;
		}
		Component toReturn = tabbedPane.getComponentAt(selected);
		if (toReturn instanceof ProjectArea) {
			return (ProjectArea) toReturn;
		}
		else {
			return null;
		}
	}

	private void initMenu() {
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		// menu options
		JMenu fileMenu = new JMenu("File");
		menuBar.add(fileMenu);

		// file menu options
		newFileOption = new JMenuItem("New");
		fileMenu.add(newFileOption);
		newFileOption.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.CTRL_DOWN_MASK));
		newFileOption.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				ProjectArea.newProjectArea();
			}
		});
		
		closeOption = new JMenuItem("Close"); 
		closeOption.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q,KeyEvent.CTRL_DOWN_MASK));
		closeOption.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(tabbedPane.getTabCount() > 0) {
					ProjectArea p = (ProjectArea)tabbedPane.getSelectedComponent();
					if(p.commandThreadRunning()) p.killCommandThread();
					tabbedPane.remove(tabbedPane.getSelectedIndex());
				}
			}
			
		});
		
		fileMenu.addMenuListener(new MenuListener() {

			@Override
			public void menuCanceled(MenuEvent arg0) {
				if(tabbedPane.getTabCount() > 0) {
					closeOption.setEnabled(true);
					exportCSVOption.setEnabled(true);
				} else {
					closeOption.setEnabled(false);
					exportCSVOption.setEnabled(false);
				}
			}

			@Override
			public void menuDeselected(MenuEvent arg0) {
				if(tabbedPane.getTabCount() > 0) {
					closeOption.setEnabled(true);
					exportCSVOption.setEnabled(true);
				} else {
					closeOption.setEnabled(false);
					exportCSVOption.setEnabled(false);
				}
			}

			@Override
			public void menuSelected(MenuEvent arg0) {
				if(tabbedPane.getTabCount() > 0) {
					closeOption.setEnabled(true);
					exportCSVOption.setEnabled(true);
				} else {
					closeOption.setEnabled(false);
					exportCSVOption.setEnabled(false);
				}
			}
			
		});
		
		// import into project option
		importFromFileOption = new JMenuItem("Import from CSV");
		fileMenu.add(importFromFileOption);
		importFromFileOption.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, KeyEvent.CTRL_DOWN_MASK));
		importFromFileOption.addActionListener(new ImportCSVFromFileAction(null));
		
		// export to csv option
		exportCSVOption = new JMenuItem("Export to CSV");
		exportCSVOption.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E,KeyEvent.CTRL_DOWN_MASK));
		exportCSVOption.addActionListener(new ExportCSVFileAction(null));
		fileMenu.add(exportCSVOption);
		
		fileMenu.add(closeOption);

		
		// open help menu option
		openHelpMenu = new JMenu("Commands");
		menuBar.add(openHelpMenu);
		
		openHelpMenu.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent arg0) {
				helpMenuGUI.displayMenu();
			}
			
		});
		
		// networking option
		networkMenu = new JMenu("Network");
		menuBar.add(networkMenu);
		networkMenu.addMenuListener(new MenuListener() {

			@Override
			public void menuCanceled(MenuEvent arg0) {
				if (online) {
					signinOption.setEnabled(false);
					signoutOption.setEnabled(true);
				}
				else {
					signinOption.setEnabled(true);
					signoutOption.setEnabled(false);
				}
			}

			@Override
			public void menuDeselected(MenuEvent arg0) {
				if (online) {
					signinOption.setEnabled(false);
					signoutOption.setEnabled(true);
				}
				else {
					signinOption.setEnabled(true);
					signoutOption.setEnabled(false);
				}
			}

			@Override
			public void menuSelected(MenuEvent arg0) {
				if (online) {
					signinOption.setEnabled(false);
					signoutOption.setEnabled(true);
				}
				else {
					signinOption.setEnabled(true);
					signoutOption.setEnabled(false);
				}
			}
			
		});
		
		// Connect to project area item
		connectOption = new JMenuItem("Connect to User");
		connectOption.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				client.new RequestUsersThread();
			}
		});
		

		// import whole project area option
		openProjectAreaOption = new JMenuItem("Open Project Area");
		openProjectAreaOption.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, KeyEvent.CTRL_DOWN_MASK));
		openProjectAreaOption.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				client.new OpenFileThread();
			}
		});
		
		
		// save project area option
		
		saveProjectAreaOption = new JMenuItem("Save Project Area");
		saveProjectAreaOption.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.CTRL_DOWN_MASK));
		saveProjectAreaOption.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ProjectArea toSave = getActiveProjectArea();
				if (toSave == null) return;
				client.new SaveFileThread((ProjectArea)tabbedPane.getSelectedComponent());
			}
		});
		
		
		// Log in (if not logged in)
		signinOption = new JMenuItem("Sign in...");
		
		// Log out (if logged in)
		signoutOption = new JMenuItem("Sign out...");
		signoutOption.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				online = false;
			}
		});
		
		
		networkMenu.add(signinOption);
		networkMenu.add(signoutOption);
		networkMenu.add(connectOption);
		networkMenu.add(openProjectAreaOption);
		networkMenu.add(saveProjectAreaOption);

		
		// open settings menu option
		openSettingsMenu = new JMenu("Settings");
		menuBar.add(openSettingsMenu);
		
		openSettingsMenu.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent arg0) {
				settingsGUI.displayMenu();
			}
			
		});

		


	}

	public static Object getClientListener() {
		// TODO Auto-generated method stub
		return client;
	}

	public static boolean isOpen(String tabName) {
		for(Component c: tabbedPane.getComponents()) {
			if(c instanceof ProjectArea && ((ProjectArea)c).getTabName().equals(tabName)) return true;
		}
		return false;
	}

	public static ProjectArea getProjectAreaByTab(String tabName) {
		for(Component c: tabbedPane.getComponents()) {
			if(c instanceof ProjectArea && ((ProjectArea)c).getTabName().equals(tabName)) return (ProjectArea) c;
		}
		return null;
	}

}
