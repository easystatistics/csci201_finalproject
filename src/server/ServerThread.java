package server;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Set;
import java.util.Vector;

import javax.swing.JPanel;

import client.FileRequest;
import client.OnlineUsersRequest;
import client.OpenFileRequest;
import client.ProjectArea;
import server.SuccessMessage.Type1;
import server.SuccessMessage.Type2;
import server.UserInfo.Type;

public class ServerThread extends Thread {
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	private static String DIRECTORY = "serverprojects/";
	private String user;
	private JPanel latestPanel;
	//private Server server;
	
	public ServerThread(Socket s, Server server) {
		try {
			//this.server = server;
			oos = new ObjectOutputStream(s.getOutputStream());
			ois = new ObjectInputStream(s.getInputStream());
			this.start();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
	
	public void sendMessage(Object message) throws IOException {
		oos.writeObject(message);
		oos.flush();
	}
	
	public void run() {
		try {
			while (true) {
				Object message;
				try {
					message = ois.readObject();
				} catch (ClassNotFoundException e1) {
					break;
				}
				if (message instanceof UserInfo) {
					UserInfo info = (UserInfo)message;
					if (info.type.equals(Type.LOGIN)) {
						System.out.println("Login: " + info.getUsername() + " " + info.getPassword());
						if (Database.get().login(info.getUsername(), info.getPassword())) {
							user = info.getUsername();
							Server.onlineThreads.put(user, this);
							sendMessage(new SuccessMessage("Login Success!", Type1.SUCCESS, Type2.LOGIN));
						}
						else {
							sendMessage(new SuccessMessage("Invalid username or password", Type1.FAILURE, Type2.LOGIN));
						}
					}
					else {
						System.out.println("Signup: " + info.getUsername() + " " + info.getPassword());
						if (Database.get().signup(info.getUsername(), info.getPassword())) {
							user = info.getUsername();
							Server.onlineThreads.put(user, this);
							sendMessage(new SuccessMessage("Signup Success!", Type1.SUCCESS, Type2.SIGNUP));
						}
						else {
							sendMessage(new SuccessMessage("User already exists.", Type1.FAILURE, Type2.SIGNUP));
						}
					}
					if(user!=null) {
						// make dir
						File dir = new File(DIRECTORY+user+'/');
						if(!dir.exists()) {
							dir.mkdir();
						}
					}
				} else if(message instanceof OpenFileRequest) {						
					Vector<String> files = new Vector<String>();
					// Get all project areas
					File[] dir = (new File(DIRECTORY+user+'/')).listFiles();
					if(dir !=null){
						for(File f : dir) if(f.isFile()) files.add(f.getName());					
					}
					// send file names to user
					sendMessage(new FileResponse(files));

				} else if(message instanceof OnlineUsersRequest) {						
					Set<String> users = Server.onlineThreads.keySet();
					users.remove(user);
					
					// send file names to user
					sendMessage(new FileResponse(new Vector<String>(users)));

				} else if (message instanceof ProjectArea) {
					ProjectArea pa = (ProjectArea) message;
					latestPanel = pa.getDisplayPanel();
			
					// Save Project Area
					String filename = DIRECTORY+user+'/'+pa.getTabName();
					if(!filename.endsWith(".pa"))filename+=".pa";

					FileOutputStream fos = new FileOutputStream(filename);
					ObjectOutputStream ostream = new ObjectOutputStream(fos);

					ostream.writeObject(pa);
					ostream.close();
					fos.close();
				
					sendMessage(new ServerResponse(true));

				} else if (message instanceof JPanel) {
					latestPanel = (JPanel) message;
					sendMessage(new ServerResponse(true));
					
				} else if (message instanceof FileRequest) {
					FileRequest fr = (FileRequest) message;
					String filename = fr.getFilename();
					String owner = fr.getOwner();
					if(owner == null) {
						// own file
						if(filename==null||filename.trim().length()==0) {
							sendMessage(null);

						} else {
							if(!filename.endsWith(".pa"))filename+=".pa";
							FileInputStream fis = new FileInputStream(new File(DIRECTORY+user+'/'+filename));
							
							ObjectInputStream istream = new ObjectInputStream(fis);
							
							Object obj = null;
							try {
								obj = istream.readObject();
							} catch (ClassNotFoundException e) {
								// TODO Auto-generated catch block
							} catch (IOException e) {
							}
							
							if(obj != null && obj instanceof ProjectArea) {
								sendMessage(obj);
							} else {
								sendMessage(null);
							}
							
							istream.close();
							fis.close();
						}
					} else {
						// other user's file
						ServerThread st = Server.onlineThreads.get(owner);
						
						if(st != null) {
							sendMessage(st.latestPanel());
						} else {
							sendMessage(null);
						}
						
					}
				
				}
			}
		} catch (IOException ioe) {
		} finally {
			if(user!=null)Server.onlineThreads.remove(user);
		}
	}

	private JPanel latestPanel() {
		// TODO Auto-generated method stub
		return latestPanel;
	}
}
