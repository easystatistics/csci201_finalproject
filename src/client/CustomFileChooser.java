package client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Vector;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class CustomFileChooser extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1L;
	Vector<String> filenames;
	String mainAction;
	private String selectedFile;
	JTextField fileInput;
	
	CustomFileChooser(JFrame parent, String mainAction, Vector<String> filenames, String initialFile, String displayText) {
	  super(parent,"File Chooser",true);

	  this.mainAction = mainAction;
      JPanel messagePane = new JPanel(new FlowLayout(FlowLayout.LEFT,0,0));
      messagePane.add(new JLabel("Select a "+displayText+":"));

      // JList
      JList<String> fileList = new JList<String>(new DefaultListModel<String>() {
		private static final long serialVersionUID = 1L;

		@Override
		public String getElementAt(int position) {
			return filenames.get(position);
		}

		@Override
		public int getSize() {
			return filenames.size();
		} 
    	  
      });
      
      fileList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      
      fileList.setBorder(new LineBorder(Color.GRAY));
      
      JPanel mainPanel = new JPanel(new BorderLayout());
      
      mainPanel.add(messagePane,BorderLayout.NORTH);
      mainPanel.add(fileList, BorderLayout.CENTER);
      getContentPane().add(mainPanel, BorderLayout.CENTER);
      
      JPanel bottomPanel = new JPanel(new BorderLayout(0,10));
      bottomPanel.add(new JLabel(displayText+":"),BorderLayout.WEST);
      
      fileInput = new JTextField();	      
      if(initialFile != null) {
    	  fileInput.setText(initialFile);
      }
      bottomPanel.add(fileInput,BorderLayout.CENTER);
      
      mainPanel.add(bottomPanel,BorderLayout.SOUTH);
      
      // Buttons
      JPanel buttonPane = new JPanel(new FlowLayout(FlowLayout.RIGHT,0,0));
      
      JButton mainButton = new JButton(mainAction); 
      mainButton.addActionListener(this);
      
      JButton cancelButton = new JButton("Cancel");
      cancelButton.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent ae) {
			CustomFileChooser.this.setVisible(false);
			CustomFileChooser.this.dispose();
		}
    	  
      });
      
      buttonPane.add(cancelButton);
      JPanel border = new JPanel();
      border.setBorder(new EmptyBorder(2,10,2,5));
      buttonPane.add(border);
      buttonPane.add(mainButton); 
      	      
      bottomPanel.add(buttonPane, BorderLayout.SOUTH);
      
      bottomPanel.setBorder(new EmptyBorder(13,0,0,0));
      mainPanel.setBorder(new EmptyBorder(8,8,8,8));
      
      fileList.addListSelectionListener(new ListSelectionListener() {

		@Override
		public void valueChanged(ListSelectionEvent lse) {				
			Object o = fileList.getModel().getElementAt(fileList.getSelectedIndex());
		  	fileInput.setText(o.toString());
		}
    	  
      });
      
      fileList.addMouseListener(new MouseAdapter() {
    	  
	      public void mouseClicked(MouseEvent mouseEvent) {
	    	  if (mouseEvent.getClickCount() >= 2) {
	    		  int index = fileList.locationToIndex(mouseEvent.getPoint());
	    		  if (index >= 0) {
	    			  Object o = fileList.getModel().getElementAt(index);
	    			  fileInput.setText(o.toString());
	    			  mainButton.doClick();
	    		  }
	    	  }
	      	}
	    });
      
      fileInput.addKeyListener(new KeyAdapter() {

		@Override
		public void keyPressed(KeyEvent ke) {
			if(ke.getKeyCode()==KeyEvent.VK_ENTER) {
				mainButton.doClick();
			}
		}
    	  
      });
      
      setDefaultCloseOperation(DISPOSE_ON_CLOSE);
      pack(); 
      
      mainButton.requestFocusInWindow();
      mainButton.requestFocus();
      
      
	  setPreferredSize(new Dimension(500,300));
	  setLocationRelativeTo(parent);
      setVisible(true);


	}
	
	public String getSelectedFile() {
		return selectedFile;
	}
	
	@Override
	public void actionPerformed(ActionEvent ae) {
		selectedFile = fileInput.getText();
		setVisible(false);
		dispose();
	}
	

}
