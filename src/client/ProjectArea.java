package client;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutionException;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingWorker;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import controller.CommandController;
import exceptions.DuplicateNameException;
import exceptions.InconsistentModelDimensionException;
import exceptions.InvalidNameException;
import exceptions.ModelCannotHaveNegativeValues;
import exceptions.ModelMustHaveCategoricalDependentVariable;
import exceptions.NotFoundException;
import exceptions.VariableContainsNonNumericException;
import exceptions.VariableMustContainValuesException;
import statistics.ListShowable;
import statistics.ModelList;
import statistics.VariableList;
import statistics.VariableList.Variable;

public class ProjectArea extends JPanel implements Serializable{
	private static final long serialVersionUID = 1L;
	private JPanel sidePanel;
	private JTextField commandLine;
	private transient ArrayList<String> commandHistoryModel;
	private String tabName;
	public VariableList variables; // Actual Variables
	public ModelList models; // Actual Models
	public transient CommandController commandController;
	private JPanel displayArea;
	private transient DisplayUpdateThread displayUpdateThread;
	private int commandHistoryNumber;
	private JPanel currentShowPanel;
	private transient boolean autoRefreshDisplay;
	private boolean online;
	private JTextField searchBar;
	private ArrayBlockingQueue<SwingWorker<JPanel,String>> queue;
	private Thread commandThread;

	public JTextField getCommandLine() {
		return commandLine;
	}
	public static ProjectArea newProjectArea() {
		return new ProjectArea("New Project Area");
	}
	/*public ProjectArea(ProjectArea pa) {
		this.sidePanel = pa.sidePanel;
		this.commandLine = pa.commandLine;
		this.tabName = pa.tabName;
		this.variables = pa.variables;
		this.models = pa.models;
		this.displayArea = pa.displayArea;
		this.currentShowPanel = pa.currentShowPanel;
		this.online = pa.online;
		this.searchBar = pa.searchBar;
	}*/
	
	
	
	public static ProjectArea openProjectArea(String tabName, VariableList inVariables, ModelList inModels) {
		ProjectArea pa = new ProjectArea(tabName);
		if(inVariables!=null) {
			for(int i = 0; i < inVariables.size(); i++) {
				try {
					pa.variables.new Variable(inVariables.get(i).returnValues(),inVariables.get(i).returnName());
				} catch (DuplicateNameException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (InvalidNameException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
		if(inModels!=null) {
			for(int i = 0; i < inModels.size(); i++) {
				pa.models.addElement(inModels.get(i));
			}
		}
		pa.models.refreshNames(null);
		pa.variables.refreshNames(null);
		return pa;
	}
	
	ProjectArea(String tabName, JPanel panel) {
		// Create elements for display area
		super(new BorderLayout());
		this.setBorder(new EmptyBorder(0,15,15,15));
		if(panel!=null) {
			displayArea = panel;
		} else {
			displayArea = new JPanel();
		}
		JPanel divide = new JPanel();
		divide.add(new JLabel("Connected to "+tabName));
		divide.setBorder(new EmptyBorder(10,0,10,0));
		add(divide, BorderLayout.NORTH);
		add(displayArea, BorderLayout.CENTER);
		this.tabName = tabName;
		online = true;
		MainGUI.tabbedPane.addTab(tabName, this);
		MainGUI.tabbedPane.setSelectedIndex(MainGUI.tabbedPane.getTabCount() - 1);


	}
	
	// new project constructor
	ProjectArea(String tabName) {
		super(new BorderLayout());
	
		// Initial options
		autoRefreshDisplay = false;
		commandHistoryNumber = 0;
		this.tabName = tabName;

		commandController = new CommandController(this);
		// add this to tabbed pane
		MainGUI.tabbedPane.addTab(tabName, this);
		
		// Create command history
		commandHistoryModel = new ArrayList<String>();

		// Create elements for display area
		displayArea = new JPanel();
		add(displayArea, BorderLayout.CENTER);

		// Create elements for side panel
		sidePanel = new JPanel(new BorderLayout());
		sidePanel.setPreferredSize(new Dimension(175, 600));
		add(sidePanel, BorderLayout.EAST);

		// Variable list
		variables = new VariableList(this);

		JScrollPane varScroller = new JScrollPane(variables.variableList);
		varScroller.setBorder(new TitledBorder("My Variables"));
		sidePanel.add(varScroller, BorderLayout.CENTER);
		
		// Model list
		models = new ModelList(this);
		
		JScrollPane modelScroller = new JScrollPane(models.modelList);
		modelScroller.setBorder(new TitledBorder("My Models"));
		sidePanel.add(modelScroller, BorderLayout.SOUTH);
		
		// queue
		queue = new ArrayBlockingQueue<SwingWorker<JPanel,String>>(100);
		commandThread = new Thread() {
			@Override
			public void run() {
				try {
					while(true) {
						SwingWorker<JPanel,String> worker = queue.take();
						worker.execute();
						while(!worker.isDone()) Thread.sleep(1);
					}
				} catch (Exception e) {
					
				}
			}
		};
		
		commandThread.start();
		
		// Set up Command Line

		commandLine = new JTextField();
		commandLine.setPreferredSize(new Dimension(100, commandLine.getPreferredSize().height + 5));
		commandLine.addKeyListener(new KeyAdapter() {

			@Override // Toggle through command history
			public void keyPressed(KeyEvent ke) {
				int num = commandHistoryNumber;
				boolean proceed = false;
				if(ke.getKeyCode()==KeyEvent.VK_UP) {
					proceed = true;
					num --;
				} else if (ke.getKeyCode()==KeyEvent.VK_DOWN){
					proceed = true;
					num ++;
				}
				if(proceed && (num >= 0 && num < commandHistoryModel.size())) {
					commandLine.setText(commandHistoryModel.get(num));
					commandHistoryNumber = num;
				} else if (proceed && num < 0) {
					commandHistoryNumber = 0;
				} else if (proceed && num >= commandHistoryModel.size()) {
					commandHistoryNumber = commandHistoryModel.size();
					commandLine.setText("");
				}
			}
			
		});
		
		
		JScrollPane commandPanel = new JScrollPane(commandLine);
		commandPanel.setBorder(new TitledBorder("Command Line"));
		this.add(commandPanel, BorderLayout.SOUTH);

		// Command Line Listener
		commandLine.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// Start a new command thread and add command to history
				String command = commandLine.getText();
				commandHistoryModel.add(command);
				commandHistoryNumber = commandHistoryModel.size();
				commandLine.setText("");
				try {
					setCurrentCommand(command);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}

		});
		
		// set up list search
		// search bar
		
		searchBar = new JTextField();
		sidePanel.add(searchBar, BorderLayout.NORTH);
		searchBar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String text = searchBar.getText();
				if(text == null) text = "";
				models.refreshNames(searchBar.getText());
				variables.refreshNames(searchBar.getText());
			}
			
		});
		searchBar.addKeyListener(new KeyAdapter() {

			@Override
			public void keyTyped(KeyEvent arg0) {
				String text = searchBar.getText();
				if(text == null) text = "";
				models.refreshNames(searchBar.getText());
				variables.refreshNames(searchBar.getText());
			}
			
			@Override
			public void keyReleased(KeyEvent arg0) {
				String text = searchBar.getText();
				if(text == null) text = "";
				models.refreshNames(searchBar.getText());
				variables.refreshNames(searchBar.getText());
			}
			
		});
		
		models.refreshNames(searchBar.getText());
		variables.refreshNames(searchBar.getText());

		// set index of tabbed pane
		MainGUI.tabbedPane.setSelectedIndex(MainGUI.tabbedPane.getTabCount() - 1);
		
		// set focus in command bar
		commandLine.requestFocus();
		
	}

	public String getTabName() {
		return tabName;
	}
	
	public void setCurrentCommand(String command) throws InterruptedException {
		SwingWorker<JPanel,String> t = new SwingWorker<JPanel,String>() {
			@Override
			public void done() {
				try {
					JPanel toDisplay = get();
					if(toDisplay != null) { // Command that requires the display
						ProjectArea.this.display(toDisplay);
						ProjectArea.this.revalidate();
						
						// Start or update thread that checks for updates
						if(displayUpdateThread == null) {
							displayUpdateThread = new DisplayUpdateThread(command);
							displayUpdateThread.start();

						} else {
							displayUpdateThread.setCommand(command);
						}
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			@Override
			protected JPanel doInBackground() throws Exception {
				// TODO Auto-generated method stub
				return commandController.getResults(command);
			}

		};
		addToQueue(t);

	}

	
	private void addToQueue(SwingWorker<JPanel, String> t) throws InterruptedException {
		queue.put(t);
	}


	private class DisplayUpdateThread extends Thread {
		private String currentCommand;
		
		DisplayUpdateThread(String currentCommand) {
			super();
			this.currentCommand = currentCommand;
		}
		
		public void setCommand(String newCommand) {
			currentCommand = newCommand;
		}
		
		@Override
		public void run() {
			while(true) {
				if(variables.hasUpdates() && autoRefreshDisplay) {
					variables.notifyUpdated();
					
					// Check for variable updates
					JPanel toDisplay = commandController.getResults(currentCommand);
					
					if(toDisplay == null) break; // error somewhere
					
					ProjectArea.this.display(toDisplay);
					ProjectArea.this.revalidate();
				}
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// Return
					return;
				}
			}
		}	
	}
	

	public void display(JPanel panel) {
		if (panel != null) {
			remove(displayArea);
			displayArea = panel;
			add(displayArea, BorderLayout.CENTER);
			if(MainGUI.client !=null) {
				MainGUI.client.new UpdateThread(panel);
			}
		}
		revalidate();
	}
	
	public ArrayList<String> getAllNames() {
		ArrayList<String> names = variables.getVariableNames();
		names.addAll(models.getModelNames());
		return names;
	}
	
	public void setCurrentShowPanel(ListShowable showable) {
		// Hide current panel
		hideCurrentShowPanel();

		// Get and display the show panel
		// Expensive operation so we do this in a thread
		Thread t = new Thread() {
			@Override
			public void run() {
				try {
					currentShowPanel = showable.panel();
				} catch (VariableContainsNonNumericException | VariableMustContainValuesException
						| InconsistentModelDimensionException | ModelMustHaveCategoricalDependentVariable | ModelCannotHaveNegativeValues e) {
					// Suppress errors since the user might edit the variable 
					e.printStackTrace();
				}
				add(currentShowPanel, BorderLayout.WEST);
				currentShowPanel.revalidate();
				revalidate();
			}
		};
		
		t.start();
	}
	
	public void hideCurrentShowPanel() {
		// Check if show panel already exists
		if(currentShowPanel != null) remove(currentShowPanel);
		currentShowPanel = null;
		revalidate();
	}
	
	public void hideShowPanel(Variable showable) {
		if(currentShowPanel != null && currentShowPanel == showable.panel()) {
			hideCurrentShowPanel();
		}
	}
	
	// Finds Variable or Model by name
	public ListShowable findByName(String name) throws NotFoundException {
		ListShowable target = null;
		try {
			target = variables.findByName(name);
		} catch (Exception e) {
			try {
				target = models.findByName(name);
			} catch (Exception e1) {
				// Not found :(
				throw new NotFoundException(name);
			}
		}
		return target;
	}
	
	public boolean getAutoRefreshDisplay() {
		return autoRefreshDisplay;
	}
	
	public void setAutoRefreshDisplay(boolean b) {
		autoRefreshDisplay = b;
	}
	
	public boolean getOnline() {
		return online;
	}
	
	public void setOnline(boolean b) {
		online = b;
	}
	
	public VariableList getVariables() {
		return variables;
	}

	public void setTabName(String selectedFile) {
		tabName = selectedFile;
	}
	public JPanel getDisplayPanel() {
		// TODO Auto-generated method stub
		return displayArea;
	}
	public boolean commandThreadRunning() {
		// TODO Auto-generated method stub
		return (commandThread!=null);
	}
	public void killCommandThread() {
		commandThread.interrupt();
	}

}
