package client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Vector;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import frame.Navigator;
import server.FileResponse;
import server.ServerResponse;
import server.SuccessMessage;
import server.SuccessMessage.Type1;
import server.SuccessMessage.Type2;
import server.UserInfo;

public class ClientListener extends Thread{
	
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	private Navigator mNav;
	private Lock lock;
	
	public ClientListener(String hostname, int port, Navigator inNav) throws UnknownHostException, IOException {
		mNav = inNav;
		lock = new ReentrantLock();
		Socket s = null;
		s = new Socket(hostname, port);
		oos = new ObjectOutputStream(s.getOutputStream());
		ois = new ObjectInputStream(s.getInputStream());
		this.start();
		
	}
	
	public void sendMessage(Object message) throws IOException {
		oos.writeObject(message);
		oos.flush();		
	}
	
	public void sendInfo(UserInfo userinfo) {
		try {
			oos.writeObject(userinfo);
			oos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void run() {
		try {
			boolean success = false;
			while (!success) {
				try{lock.lock();
					SuccessMessage message = (SuccessMessage)ois.readObject();
					if (message.type1.equals(Type1.SUCCESS) && message.type2.equals(Type2.LOGIN)) {
						success = true;
						mNav.loginSuccess(message.getMessage());
					}
					if (message.type1.equals(Type1.FAILURE) && message.type2.equals(Type2.LOGIN)) {
						mNav.loginFailure(message.getMessage());
					}
					if (message.type1.equals(Type1.SUCCESS) && message.type2.equals(Type2.SIGNUP)) {
						success = true;
						mNav.signupSuccess(message.getMessage());
					}
					if (message.type1.equals(Type1.FAILURE) && message.type2.equals(Type2.SIGNUP)) {
						mNav.signupFailure(message.getMessage());
					}
				}finally {
					lock.unlock();
				}
			}
		} catch (ClassNotFoundException cnfe) {
		} catch (IOException ioe) {
		}
	}
	
	// Connection to server class
	public class OpenFileThread extends Thread {
		private boolean dontHog;		
		public OpenFileThread() {
			dontHog = true;
			this.start();
			
			while(dontHog) { // Don't hog cpu
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					break;
				}
			}
			
		}
		
		public void run() {
			Vector<String> files = null;
			lock.lock();
			try {
				sendMessage(new OpenFileRequest());
				files = ((FileResponse) ois.readObject()).getFiles();


			} catch (IOException e) {
				JOptionPane.showMessageDialog(null, "Unable to connect to the server.", "", JOptionPane.ERROR_MESSAGE);

			} catch (ClassNotFoundException e) {
				JOptionPane.showMessageDialog(null, "Error connecting to the server.", "", JOptionPane.ERROR_MESSAGE);

			} finally {
				// hog CPU since we have a pop up
				lock.unlock();
				dontHog = false;
				
			}
			

			// Show filenames to user
			if(files == null) return;
			
			CustomFileChooser fc = new CustomFileChooser(null,"Open",files,null,"file");
			String selectedFile = fc.getSelectedFile();
			if(selectedFile!=null) {
				// Server request
				new DownloadFileThread(selectedFile);
			}
		}
		

		
	}
	
	
	// Connection to server class
	public class RequestUsersThread extends Thread {
		private boolean dontHog;		
		public RequestUsersThread() {
			dontHog = true;
			this.start();
			
			while(dontHog) { // Don't hog cpu
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					break;
				}
			}
			
		}
		
		public void run() {
			Vector<String> files = null;
			lock.lock();
			try {
				sendMessage(new OnlineUsersRequest());
				files = ((FileResponse) ois.readObject()).getFiles();


			} catch (IOException e) {
				JOptionPane.showMessageDialog(null, "Unable to connect to the server.", "", JOptionPane.ERROR_MESSAGE);

			} catch (ClassNotFoundException e) {
				JOptionPane.showMessageDialog(null, "Error connecting to the server.", "", JOptionPane.ERROR_MESSAGE);

			} finally {
				// hog CPU since we have a pop up
				lock.unlock();
				dontHog = false;
				
			}
			

			// Show filenames to user
			if(files == null) return;
			
			CustomFileChooser fc = new CustomFileChooser(null,"Open",files,null,"user");
			String selectedFile = fc.getSelectedFile();
			if(selectedFile!=null) {
				// Server request
				new StreamUserThread(selectedFile);
			}
		}
		

		
	}
	
	// Connection to server class
	public class SaveFileThread extends Thread {

		private ProjectArea projectArea;
		private boolean dontHog;
		
		public SaveFileThread(ProjectArea projectArea) {
			this.projectArea = projectArea;
			
			dontHog = true;
			this.start();
			
			while(dontHog) { // Don't hog cpu
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					break;
				}
			}
			
		}
		
		public void run() {
			Vector<String> files = null;
			lock.lock();
			try {
				sendMessage(new OpenFileRequest());

				files = ((FileResponse) ois.readObject()).getFiles();


			} catch (IOException e) {
				JOptionPane.showMessageDialog(null, "Unable to connect to the server.", "", JOptionPane.ERROR_MESSAGE);

			} catch (ClassNotFoundException e) {
				JOptionPane.showMessageDialog(null, "Error connecting to the server.", "", JOptionPane.ERROR_MESSAGE);

			} finally {
				lock.unlock();
				// hog CPU since we have a pop up
				dontHog = false;

			}

			// Show filenames to user
			if(files == null) return;
			String fileName = null;
			if(MainGUI.tabbedPane.getSelectedIndex() >= 0) {
				fileName = MainGUI.tabbedPane.getTitleAt(MainGUI.tabbedPane.getSelectedIndex());
			}
			CustomFileChooser fc = new CustomFileChooser(null,"Save",files,fileName,"file");
			String selectedFile = fc.getSelectedFile();
			if(selectedFile!=null) {
				// Server request
				projectArea.setTabName(selectedFile);
				new UploadFileThread(projectArea);
			}
		}
	
		

		
	}
	
	// Connection to server class
	public class UploadFileThread extends Thread {

		private boolean dontHog;
		private ProjectArea projectArea;
		
		public UploadFileThread(ProjectArea pa) {
			this.projectArea = pa;
			
			dontHog = true;
			this.start();
			
			while(dontHog) { // Don't hog cpu
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {

				}
			}
			
		}
		
		public void run() {
			ServerResponse message = null;
			lock.lock();
			try {
				sendMessage(projectArea);
				
				Object check = ois.readObject();

				message = (ServerResponse) check;
				



			} catch (IOException e) {
				JOptionPane.showMessageDialog(null, "Unable to connect to the server.", "", JOptionPane.ERROR_MESSAGE);

			} catch (ClassNotFoundException e) {
				JOptionPane.showMessageDialog(null, "Error connecting to the server.", "", JOptionPane.ERROR_MESSAGE);

			} finally {
				lock.unlock();
				// hog CPU since we have a pop up
				dontHog = false;
			}
			

			
			if(message != null && message.wasSuccessful()) {
				JOptionPane.showMessageDialog(null, "Project Area successfully saved.", "Message", JOptionPane.INFORMATION_MESSAGE);
				String tabName = projectArea.getTabName();
				MainGUI.tabbedPane.setTitleAt(MainGUI.tabbedPane.getSelectedIndex(), tabName);
			
			} else {
				JOptionPane.showMessageDialog(null, "Project Area failed to save.", "Error", JOptionPane.ERROR_MESSAGE);

			}
			
		}

		

	}
	
	
	
	// Connection to server class
	public class DownloadFileThread extends Thread {
		private String filename;
		private boolean dontHog;
		
		public DownloadFileThread(String filename) {
			this.filename = filename;
			
			dontHog = true;
			this.start();
			
			while(dontHog) { // Don't hog cpu
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					break;
				}
			}
			
		}
		
		public void run() {
			ProjectArea toShow = null;
			lock.lock();
			try {
				sendMessage(new FileRequest(filename));

				toShow = (ProjectArea) ois.readObject();



			} catch (IOException e) {
				JOptionPane.showMessageDialog(null, "Unable to connect to the server.", "", JOptionPane.ERROR_MESSAGE);

			} catch (ClassNotFoundException e) {
				JOptionPane.showMessageDialog(null, "Error connecting to the server.", "", JOptionPane.ERROR_MESSAGE);

			} finally {
				lock.unlock();
				// hog CPU since we have a pop up
				dontHog = false;
			}
			

			
			if(toShow == null) {
				JOptionPane.showMessageDialog(null, "Project Area failed to open.", "Error", JOptionPane.ERROR_MESSAGE);
			} else {
				ProjectArea.openProjectArea(toShow.getTabName(),toShow.variables,toShow.models);
			}
			
		}
		
		

	}
	
	// Connection to server class
		public class StreamUserThread extends Thread {
			private String owner;
			private boolean dontHog;
			
			public StreamUserThread(String owner) {
				this.owner=owner;
				dontHog = true;
				this.start();
				
				while(dontHog) { // Don't hog cpu
					try {
						Thread.sleep(200);
					} catch (InterruptedException e) {
						break;
					}
				}
				
			}
			
			public void run() {
				Object toShow = null;
				lock.lock();
				try {
					sendMessage(new FileRequest(null,owner));

					toShow =  ois.readObject();

					

				} catch (IOException e) {
					JOptionPane.showMessageDialog(null, "Unable to connect to the server.", "", JOptionPane.ERROR_MESSAGE);

				} catch (ClassNotFoundException e) {
					JOptionPane.showMessageDialog(null, "Error connecting to the server.", "", JOptionPane.ERROR_MESSAGE);

				} finally {
					lock.unlock();
					// hog CPU since we have a pop up
				}
				
				JPanel panel = null;
				ProjectArea project = null;

				panel = (JPanel)toShow;
				project = new ProjectArea(owner,panel);

				
				while(MainGUI.isOpen(owner)) {
					if(MainGUI.tabbedPane.getTitleAt(MainGUI.tabbedPane.getSelectedIndex()).equals(owner)) {
	
						lock.lock();
						try {
							sendMessage(new FileRequest(null,owner));
							toShow =  ois.readObject();
						} catch (ClassNotFoundException e) {
							// TODO Auto-generated catch block
							dontHog = false;
							return;
						} catch (IOException e) {
							// TODO Auto-generated catch block
							dontHog = false;
							return;
						} finally {
							lock.unlock();
						}
							
						project.display((JPanel)toShow);
						project.repaint();
						project.revalidate();
						
					}
					
					try {
						Thread.sleep(200);
					} catch (InterruptedException e) {
						dontHog = false;
						return;
					}
					
					
				}
				try {
					
				} finally {
					dontHog = false;
				}

				
			}
			
			

		}
	
	// Connection to server class
	public class UpdateThread extends Thread {
		private JPanel panel;
		private boolean dontHog;
		
		public UpdateThread(JPanel panel) {
			this.panel = panel;
			dontHog = true;
			this.start();
			
			while(dontHog) { // Don't hog cpu
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					break;
				}
			}
			
		}
		
		public void run() {
			lock.lock();
			try {
				sendMessage(panel);

				ois.readObject();


			} catch (IOException e) {
				JOptionPane.showMessageDialog(null, "Unable to connect to the server.", "", JOptionPane.ERROR_MESSAGE);
			} catch (ClassNotFoundException e) {
				JOptionPane.showMessageDialog(null, "Error connecting to the server.", "", JOptionPane.ERROR_MESSAGE);
			} finally {
				lock.unlock();
				// hog CPU since we have a pop up
				dontHog = false;
			}
			
		}
		
		

	}
	

}
