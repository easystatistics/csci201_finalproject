package client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

public class ExportCSVFileAction implements ActionListener {
	private ProjectArea _projectArea;
	
	public ExportCSVFileAction(ProjectArea projectArea) {
		_projectArea = projectArea;
	}
	
	
	public void actionPerformed(ActionEvent ae) {
		ProjectArea projectArea = _projectArea;
		JFileChooser file_chooser = new JFileChooser();
		file_chooser.setName("Save File...");
		file_chooser.setAcceptAllFileFilterUsed(false);
		file_chooser.setFileFilter(new FileNameExtensionFilter(".csv files (*.csv)", "csv"));
		if (file_chooser.showSaveDialog(MainGUI.tabbedPane) == JFileChooser.APPROVE_OPTION) {				
			CSVExporter.parseVariableList(file_chooser.getSelectedFile(), projectArea);
		}
	}
}
