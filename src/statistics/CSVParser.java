package statistics;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import com.opencsv.CSVReader;

import client.ProjectArea;
import exceptions.DuplicateNameException;
import exceptions.InvalidNameException;
import resources.Config;
import statistics.VariableList.Variable;

public class CSVParser extends SwingWorker<Integer,String>{
	private File file;
	private ProjectArea projectArea;
	private CSVReader r;
	
	public CSVParser(ProjectArea pa, File f) {
		this.projectArea = pa;
		this.file = f;
	    //  Check the file
		if (file == null) {
			// no file
			projectArea.display(projectArea.commandController.defaultPanel("Parse Error", "Please enter a valid file", true));
			return;
		}
		if (!file.getName().endsWith(".csv")) {
			// bad file extension
			projectArea.display(projectArea.commandController.defaultPanel("Parse Error", file.getName()+" is not a csv file.", true));
			return;
		}
		this.execute();

	}
	private void parseVariablesFromFileHelper(CSVReader r) throws IOException {
		
		// First row contains variable names
		String[] varNames = r.readNext();
		Variable[] newVars = new Variable[varNames.length];

		// create the variables and handle variable name errors
		for (int i = 0; i < varNames.length; i++) {
			// Handle duplicate/Invalid variable names on the fly
			try {
				Runner run  = new Runner(varNames[i]); 
				SwingUtilities.invokeAndWait(run);
				if(run.response==null) {return;}
				else { newVars[i] = run.response; }
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}

		// get these values
		String[] nextValues;
		
		
		while ((nextValues = r.readNext()) != null) {

			for (int i = 0; i < varNames.length; i++) {
				if (i < nextValues.length && nextValues[i] != null && newVars[i] != null) {
					publish(nextValues[i]);
					if(!Config.isNullValuePolicyStrict()) {
						newVars[i].add(nextValues[i]);
					} else {
						try{
							Double.parseDouble(nextValues[i]);
							newVars[i].add(nextValues[i]);
						} catch (NumberFormatException nfe) {
							// Do nothing
						}
					}
				
				}
			}
		}
	
	}
	
	class Runner implements Runnable {
		Variable response;
		String name;
		Runner(String name) {
			this.name = name;
		}
		@Override
		public void run() {
			try {
				response = projectArea.variables.new Variable(new ArrayList<Double>(), name);
			} catch (DuplicateNameException e) {
				// Duplicate variable name found
				projectArea.display(projectArea.commandController.defaultPanel("Parse Error", e.getMessage(), true));
				return;
			} catch (InvalidNameException e) {
				// Invalid variable name found
				projectArea.display(projectArea.commandController.defaultPanel("Parse Error", e.getMessage(), true));
				return;
			}
		}
	}
	
	// The method that actually does calls the import parser
	@Override
	protected Integer doInBackground() throws Exception {
		try {
			// import variables
			r = new CSVReader(new FileReader(file));
			parseVariablesFromFileHelper(r);
		} catch (IOException e) {
			// File exception
			projectArea.display(projectArea.commandController.defaultPanel("Parse Error", "Error reading from "+file.getName()+".", true));
			return 1;
		} finally {
			if (r!=null) {
				try {
					r.close();
				} catch (IOException e) {
					// Error closing file reader
				}
			}
		}
		// TODO Auto-generated method stub
		return 0;
	}
	  
      @Override
      protected void done() {
          try {
              if(this.get()==0) {
            	  // good
    			  projectArea.variables.refreshNames(null);
    			  projectArea.models.refreshNames(null);
              } else {
            	  //bad
              }
          } catch (Exception ignore) {
          }
      }

}
