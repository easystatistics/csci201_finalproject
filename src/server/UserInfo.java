package server;

import java.io.Serializable;

public class UserInfo implements Serializable{
	private static final long serialVersionUID = 1L;
	private String username;
	private String password;
	public Type type;
	
	public enum Type {
		SIGNUP, LOGIN
	}
	
	public UserInfo (String username, String password, Type type) {
		this.username = username;
		this.password = password;
		this.type = type;
	}
	
	public String getUsername() {
		return username;
	}
	
	public String getPassword() {
		return password;
	}
}
