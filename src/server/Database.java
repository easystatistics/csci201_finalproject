package server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Driver;

public class Database {
	
	private static Database sDatabase;
	public static Database get() {
		return sDatabase;
	}
	
	static {
		sDatabase = new Database();
	}
	
	private final static String newAccount = "INSERT INTO users(username,password) VALUES(?,?)";
	private final static String selectUser = "SELECT password FROM users WHERE username=?";
	
	private Connection con;
	
	{
		try{
			new Driver();
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/easyStatisticsDB?user=root&password=root&useSSL=false");
		} catch(SQLException sqle){
			System.out.println("SQL:"+sqle.getMessage());
		}
	}
	
	public void stop() {
		try {con.close();} catch (SQLException e) {e.printStackTrace();}
	}
	
	public boolean signup(String username, String password) {
		try {
			{ //check if the user exists
				PreparedStatement ps = con.prepareStatement(selectUser);
				ps.setString(1,username);
				ResultSet result = ps.executeQuery();
				if(result.next()) {//if we have any results, don't let the user sign up.
					return false;
				}
			}
			{ //sign up
				PreparedStatement ps = con.prepareStatement(newAccount);
				ps.setString(1, username);
				ps.setString(2, password);
				ps.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean login(String username, String password) {
		try {
			PreparedStatement ps = con.prepareStatement(selectUser);
			ps.setString(1,username);
			ResultSet result = ps.executeQuery();
			while(result.next()) {
				if(password.equals(result.getString("password"))) return true;
			}
		} catch (SQLException e) {return false;}
		return false;
	}
	
	public void toggleHost() {
		
	}

}
