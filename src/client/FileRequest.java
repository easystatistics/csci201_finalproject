package client;

import java.io.Serializable;

public class FileRequest implements Serializable {
	private static final long serialVersionUID = 1L;
	private String filename;
	private String owner;
	
	FileRequest(String filename, String owner) {
		this.filename = filename;
		this.owner = owner;
	}
	
	FileRequest(String filename) {
		this.filename = filename;
	}
	
	
	public String getFilename() {
		return filename;
	}


	public String getOwner() {
		// TODO Auto-generated method stub
		return owner;
	}
	
	
}
